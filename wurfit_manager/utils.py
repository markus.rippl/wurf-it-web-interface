from flask import render_template
from .models import Server
from .rest import rest

def base_render_template(template_name_or_list, **context):
    if 'servers' not in context:
        context['servers'] = servers=Server.query.all()
    if 'rest_available' not in context:
        context['rest_available'] = rest.is_available()

    return render_template(template_name_or_list, **context)

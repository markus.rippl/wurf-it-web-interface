from flask import Blueprint
from .utils import base_render_template

import netifaces

interfaces = Blueprint('interfaces', __name__, template_folder='templates')


def interface_list():
    result = {}
    for interface in  netifaces.interfaces():
        if netifaces.AF_INET not in netifaces.ifaddresses(interface):
            continue
        for address in netifaces.ifaddresses(interface)[netifaces.AF_INET]:
            result[interface] = address['addr']
    return result


@interfaces.route('/interfaces/', methods=['GET'])
def show():
    return base_render_template('interfaces.html', interfaces=interface_list())

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_apscheduler import APScheduler

from wurfit_manager.models import db, Server
from wurfit_manager.momentjs import momentjs
from wurfit_manager.config import Config
from wurfit_manager.rest import rest

from wurfit_manager.config_schema import config_schema

from wurfit_manager.utils import base_render_template

from wurfit_manager.files import files
from wurfit_manager.interfaces import interfaces
from wurfit_manager.servers import servers

import os
import requests
import logging
logging.basicConfig()

def init_db():
    db.create_all()
    db.session.commit()
    print('Initialized the database.')

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    if test_config is not None:
        # load the test config if passed in
        app.config.from_mapping(test_config)


    if app.config['SQLALCHEMY_DATABASE_URI'] is None:
        app.config['SQLALCHEMY_DATABASE_URI'] = \
            'sqlite:///' + os.path.join(app.instance_path, 'app.db')

    if app.config['DEBUG']:
        app.config['TEMPLATES_AUTO_RELOAD'] = True
        app.jinja_env.auto_reload = True

    # Initialize Bootstrap
    bootstrap = Bootstrap(app)

    # Initialize SQLAlchemy
    db.app = app
    db.init_app(app)

    # Initialize Rest
    rest.init_app(app)

    # Initialize APScheduler
    scheduler = APScheduler()
    scheduler.api_enabled = True
    scheduler.init_app(app)

    @scheduler.task('interval', id='auto_start', seconds=15, coalesce=True)
    def auto_start():

        servers = Server.query.filter(Server.auto_start == True).all()
        print("Running auto start - found {} auto start servers.".format(len(servers)))
        try:
            running_servers = rest.get_servers()
            running_servers = running_servers.json()
        except requests.exceptions.RequestException as e:
            print(e)
            running_servers = { }
        for server in servers:
            if server.id not in running_servers:
                response, code = server.start()
                if code != 201:
                    print("Unable to start server {}: {}".format(
                        server.id, response))
        db.session.commit()

    scheduler.start()

    # Set jinja template global
    app.jinja_env.globals['momentjs'] = momentjs
    app.jinja_env.filters['hash'] = hash

    try:
        os.makedirs(app.config['UPLOAD_FOLDER'])
    except OSError:
        pass

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.cli.command('initdb')
    def initdb_command():
        """Initializes the database."""
        init_db()

    @app.cli.command('routes')
    def list_routes():
        import urllib
        output = []
        for rule in app.url_map.iter_rules():

            options = {}
            for arg in rule.arguments:
                options[arg] = "[{0}]".format(arg)

            methods = ','.join(rule.methods)
            url = url_for(rule.endpoint, **options)
            line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
            output.append(line)

        for line in sorted(output):
            print(line)

    @app.route('/', methods=['GET', 'POST'])
    def index():
        return base_render_template('index.html')

    app.register_blueprint(files)
    app.register_blueprint(interfaces)
    app.register_blueprint(servers)

    @app.errorhandler(404)
    def page_not_found(e):
        return base_render_template('404.html'), 404

    @app.errorhandler(500)
    def page_not_found(e):
        return base_render_template('500.html'), 500

    return app

from flask import Blueprint, request, current_app, flash, redirect, url_for
from werkzeug.utils import secure_filename
from .utils import base_render_template

import os

files = Blueprint('files', __name__, template_folder='templates')

def file_list():
    upload_folder = current_app.config['UPLOAD_FOLDER']
    return [os.path.join(upload_folder, f) for f in os.listdir(upload_folder) \
        if os.path.isfile(os.path.join(upload_folder, f))]


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']

@files.route('/files/', methods=['GET', 'POST'])
def show():

    while True: # while is just to have a scope
        if request.method != 'POST':
            break

        # check if the post request has the file part
        if 'file' not in request.files:
            break

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            break

        if not allowed_file(file.filename):
            flash('Not a valid files type. valid file types are: {}'.format(
                ', '.join(current_app.config['ALLOWED_EXTENSIONS'])), 'danger')
            break

        filename = secure_filename(file.filename)
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        flash('{} uploaded.'.format(filename), 'info')
        break

    return base_render_template('files.html', files=file_list())

@files.route('/files/delete', methods=['GET'])
def delete():
    file = os.path.basename(request.args.get("file"))
    file_to_remove = os.path.join(current_app.config['UPLOAD_FOLDER'], file)
    flash('{} deleted.'.format(file_to_remove), 'info')
    print("Removing {} file to remove".format(file_to_remove))
    os.remove(file_to_remove)
    return redirect(url_for('files.show'))
import os
import binascii
from flask import current_app as app

class Config(object):

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or None
    TESTING= os.environ.get('WURF_IT_TESTING') or True
    DEBUG= os.environ.get('WURF_IT_DEBUG') or True
    UPLOAD_FOLDER= os.environ.get('WURF_IT_UPLOAD_FOLDER') or \
        '/tmp/wurf_it/uploads/'
    ALLOWED_EXTENSIONS= os.environ.get('WURF_IT_ALLOWED_EXTENSIONS') or \
        ['mp4', 'sdp']
    WURF_IT_REST_IP= os.environ.get('WURF_IT_REST_IP') or '127.0.0.1'
    WURF_IT_REST_PORT= os.environ.get('WURF_IT_REST_PORT') or 8282
    BOOTSTRAP_SERVE_LOCAL= os.environ.get('WURF_IT_BOOTSTRAP_SERVE_LOCAL') or \
        True
    SECRET_KEY= os.environ.get('WURF_IT_SECRET_KEY') or \
        binascii.hexlify(os.urandom(32))

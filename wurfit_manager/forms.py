import json
import jsonschema

from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, SubmitField, TextAreaField
from wtforms import ValidationError
from wtforms.validators import InputRequired, Length

from .config_schema import config_schema
from .models import db, Server

class WurfItConfig(object):

    def __call__(self, form, field):
        try:
            j = json.loads(field.data)
            jsonschema.validate(j, config_schema(), format_checker=jsonschema.FormatChecker())
        except ValueError as e:
            raise ValidationError(u'Field must be a valid json string: {}'.format(e))
        except jsonschema.ValidationError as e:
            print(e)
            message = e.message
            if "type" in e.instance:
                t = e.instance['type']
                print(t)
                print(e.validator_value)
                print([list(v.values())[0] for v in e.validator_value])
                valid_types = [list(v.values())[0].split('/')[-1] for v in e.validator_value]
                if t not in valid_types:
                    message = "{} is not a valid type. Valid types are: {}".format(t, ', '.join(valid_types))

            raise ValidationError(u'Field must be a valid wurf.it config: {}'.format(message))

class ServerForm(FlaskForm):

    name = StringField('Name', validators=[InputRequired(), Length(1, 80)])
    config_string = TextAreaField(
        'Config Preview',
        validators=[InputRequired(), WurfItConfig()],
        id='config-text-area',
        render_kw={
            'rows': '8',
            'style': "font-family:monospace;"
        },
        default="{ }")

    auto_start = BooleanField(
        'Auto start',
        description='Constructs and start the server automatically.',
        default=False)

    def __init__(self, original_name, *args, **kwargs):
        super(ServerForm, self).__init__(*args, **kwargs)
        self.original_name = original_name

    def validate_name(self, name):
        if name.data != self.original_name or self.original_name is None:
            server = Server.query.filter_by(name=self.name.data).first()
            if server is not None:
                raise ValidationError('Please use a different name.')

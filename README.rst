=====================
Wurf.it Web Interface
=====================

The wurf-it web interface allows users to manage configuration files,
media files, and servers in the browser.

Setup
-----

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
    flask initdb

Usage
-----

The server can now be run with :

    flask run

The servers options can be changed with the following environment variables:

* DATABASE_URL  (DEFAULT: 'sqlite:///[REPOSITORY_FOLDER]/app.db'))
* WURF_IT_TESTING  (DEFAULT: True)
* WURF_IT_DEBUG  (DEFAULT: True)
* WURF_IT_CONFIGS_FILE  (DEFAULT: '/tmp/wurf_it/configs.json')
* WURF_IT_UPLOAD_FOLDER  (DEFAULT: '/tmp/wurf_it/uploads/')
* WURF_IT_ALLOWED_EXTENSIONS  (DEFAULT: ['mp4', 'sdp'])
* WURF_IT_REST_IP  (DEFAULT: '127.0.0.1')
* WURF_IT_REST_PORT  (DEFAULT: 8282)
* WURF_IT_BOOTSTRAP_SERVE_LOCAL  (DEFAULT: True)
